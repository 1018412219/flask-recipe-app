## Title

Scones

## Description

Scones feel like a special treat for breakfast!  These scones have a slightly crunchy exterior and a delicious, buttery interior.

These scones don't contain any filling, like raisins or other dried fruit, which is how I prefer my scones.  I prefer to add jam or butter to my scones.

This recipe is adapted from Paul Hollywood's (judge on the Great British Baking Show) YouTube video: <a href="https://youtu.be/-Fet_XHruSQ">Paul Hollywood's Easy Bakes - Scones</a>

## Image

<img src="{{ url_for('static', filename='img/scones.jpg') }}" alt="Scones" />

## Tips

After mixing the flour and butter, add the remaining dry ingredients and mix well with a whisk.  This step makes sure that the baking powder is well distributed.

The bake time of 15 minutes has always worked well for me.  The tops of the scones may seem slightly light in color, but I find that they darken a bit more after taking the scones out of the oven.  Plus, any additional time will really dry out the scones and not give them as nice of a texture when biting into them.

## Ingredients

* 500 grams (plus 1/2 cup) of Bread Flour
* 50 grams of Dairy-Free Butter (recommendation: [Earth Balance - Soy Free Butter Spread](https://www.earthbalancenatural.com/spreads/soy-free-buttery-spread))
* 80 grams of Granulated Sugar
* 1/2 teaspoon of Sea Salt (fine)
* 5 teaspoons of Baking Powder
* 2 large Eggs
* 250 milliliters (ml) of Almond Milk (unsweetened)

## Steps

1. Preheat oven to 400°F.
2. In a large bowl, add the flour and butter.  Using your hands, squish the butter into the flour to break up all the butter into the flour.  This step should take a few minutes to get all the butter and flour mixed into a sand-like consistency.
3. Add the remaining dry ingredients (sugar, salt, baking powder) to the bowl and mix thoroughly with a whisk.
4. Add the wet ingredients (eggs, almond milk) and mix the dough together in the bowl with a wooden spoon.
5. Add approximately 1/2 cup of bread flour to a clean surface (countertop or cutting board).  Pour the dough out and fold the dough onto itself about 10 times to form a smooth dough ball.  Make sure to not knead the dough (a stretching motion) as that will make the dough tough when it cooks.
6. Flatten the dough out with your hands or a rolling pin to a 1" thickness.
7. Using a round biscuit cutter (approximately 3" in diameter), cut out the scones and place each one onto a baking sheet with a <a href="https://www.bedbathandbeyond.com/store/product/silpat-reg-silpain-reg-silicone-bread-baking-mat/1044058814">SilPain</a> or parchment paper.  This dough should make 8-12 scones.
8. Bake for 15 minutes.
9. Let the scones cool for 5-10 minutes before eating plain or topping with jam or butter.  Enjoy!
